function getText(e) {
  return e.firstChild.firstChild.innerText;
}

function fixWidth() {
  var numChars = 0;
  $('.child_links li').each(function() {
    console.log(this);
    numChars += getText(this).length+.001;
  });
  $('.child_links li').each(function() {
    var percent = 'width: '+(getText(this).length/numChars*100)+'%;';
    $(this).attr('style', percent);
  });
};

window.addEventListener('DOMContentLoaded', fixWidth, false);

//setTimeout(fixWidth, 100);
