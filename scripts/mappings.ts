/*
  Mappings

  Mappings are matchers that we use to determine if we should execute a
  bit of Tritium during an execution. Aka, run something when we are
  are on a certain page.

  Example starting code:
*/

match($status) {

  with(/302/) {
    log("--> STATUS: 302") # redirect: just let it go through
  }
  
  with(/200/) {
    log("--> STATUS: 200")

    match($path) {
      with(/home\/home\.asp/) {
        log("--> Importing pages/home.ts in mappings.ts")
        @import pages/home.ts
      }
      with(/helpcentre\/.*?q=/) {
        log("--> Importing pages/search.ts in mappings.ts")
        @import pages/search.ts
      }
      with(/\/creditcards\//) {
        log("--> Importing pages/cc/cc_landing.ts in mappings.ts")
        @import pages/cc/cc_landing.ts
      }
      with(/personal\/a/) {
        log("--> Importing pages/cc/cc_signup.ts in mappings.ts")
        @import pages/cc/cc_signup.ts
      }
      else() {
        log("--> No page match in mappings.ts")
#        $(".//body") {
#          $("./*") {
#            attributes(style: "display: none;")
#          }
#          insert_before("a", "These pages under construction!  Tap here to see the unfinished site", style: "padding: 10px;", onclick: "$('body > * > *').attr('style', 'display: block;');this.remove();")
#        }
      }
    }
  }

  else() {
    # not 200 or 302 response status
    log("--> STATUS: " + $status + " assuming its an error code pages/error.ts")
    @import pages/error.ts
  }

}
