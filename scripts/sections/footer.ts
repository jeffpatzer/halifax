#HALIFAX FOOTER USES THEIR OWN JAVASCRIPT
$(".//div[@id='footerlinks']") {
  add_class("mw-footer")
  $("./div[@id='explore']") {
    attributes(ontouchstart: "$('#exploreheader')[0].click();")
  }
  $(".//div[@class='footerrow' or @id='footer_link_panel_container']") {
    $num_cols = '0'
    $("./div[contains(@class, 'footer_link_panel')]") {
      $("./ancestor::div[1]") {
        remove_class("cols-"+$num_cols)
      }
      $num_cols = index()
      $("./ancestor::div[1]") {
        add_class("cols-"+$num_cols)
      }
    }
  }
}

