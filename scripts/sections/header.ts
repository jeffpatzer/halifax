$(".//div[@id='masthead']") {
  add_class("mw-header")
  remove(".//input[@type='image']")
  ### LOGO AREA AND SEARCH BAR SETUP ###
  $("./div[@id='logo_area']") {
    move_here(".//a[contains(@href, 'branchfinder')]", "top") {
      add_class("mw-btn1")
      insert_top("div", class: "sprites-pin")
    }
    move_here(".//a[contains(@href, 'contactushome.asp')]", "top") {
      add_class("mw-btn1 mw-contactus")
      insert_top("div", class: "sprites-call")
    }
    move_here(".//a[contains(@title, 'Sign in')]") {
      add_class("mw-btn2")
      text("Sign In")
    }
    move_here(".//form", "top") {
      add_class("mw-searchbar")
      insert_before("a", class: "sprites-search")
    }
    
    move_here(".//div[@id='logo_panel']/a", "top") {
      add_class("mw-logo")
    }
    remove("./div")
    wrap_together("./*[position() > 1]", "div") {
      add_class("mw-header-btns")
    }
  }
  ### END LOGO AREA SETUP ###

  ### GENERAGE BREADCRUMB FOR FOLLOWING PAGES ###
  remove(".//span[@id='home_call_out_link']|.//div[@id='layer']") 
  $(".//div[contains(@class, 'parent_links')]//a") {
    text() {
      replace(/\scredit\scards?/, '')
    }
  }
  $(".//div[contains(@class, 'parent_links')]//li[position()>1]") {
    $(".//a") {
      attributes(href: "", style: "opacity: .6;")
    }
    $("./following::div[@id='child_nav'][1]") {
      $(".//a") {
        attributes(href: "", style: "opacity: .6;")
      }
    }
  }
  $(".//span[contains(@class, 'child_tab_container')]/ancestor::li[1]") {
    add_class("mw-current")
  }
  ### END BREADCRUMB ###
  
  ### LINK FORMATTING ###
  $(".//span[contains(@class, 'child_link')]/a") {
    text() {
      replace(/\scredit\scards?/, '')
    }
  }
  ### END LINKS ###
}
