$("./body") {
  add_class("mw-ccsignup")
  $(".//p[@id='logo']/span") {
    inner_wrap("a", href: "/")
  }

  $(".//a[@class='newwin']") {
    inner_wrap("span", class: "mw-secure-text")
  }
  $(".//input[@type='password']") {
    add_class("mw-input")
  }
  $(".//h2[contains(@class,'bigHead')]") {
    match(fetch("./text()")) {
      with(/Responsible lending/i) {
        $("./ancestor::div[1]") {
          add_class("mw-full")
        }
      }
      with(/Agree to checks and your data usage/i) {
        $("./ancestor::div[1]") {
          add_class("mw-full")
        }
      }
    }
  }
  remove(".//div[contains(@class, 'pageWrap')]//br")
}
