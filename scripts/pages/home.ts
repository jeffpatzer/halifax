#URL=www.halifax.co.uk/home/home.asp
$("./body") {
  add_class("mw-home")
  remove(".//div[@id='parent_nav']")
  $(".//div[@id='box']") {
    attributes(style: "width: 100%;")
    $(".//div[@id='content']") {
      add_class("mw-body")
      $(".//div[@id='secondarycontent']") {
        insert("div", class: "mw-row") {
          move_here("../div[contains(@class,'SSIpodcolumn')]/*[1]")
        }
        insert("div", class: "mw-row") {
          move_here("../div[contains(@class,'SSIpodcolumn')]/*[1]")
        }
        remove("./*[not(contains(@class, 'mw-'))]")
        $(".//div[contains(@class, 'servicepod')]") {
          attributes(class: "mw-servicepod")
        }
      }
    }
  }
  $(".//a[@href='/creditcards/']") {
    wrap_text_children("span") {
      text() {
        replace(/thatsuits/, 'that suits')
      }
    }
    $("./ancestor::div[@class='SSIpodcontainer']") {
      add_class("mw-visible")
    }
  }

  #Fix button position in tiles
  $(".//div[contains(@class, 'SSIpodcontainer')]") {
    move_here(".//div[contains(@class, 'buttons')]")
  }
}
